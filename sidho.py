import os
import json
import sklearn
import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression,SGDClassifier,PassiveAggressiveClassifier
import pandas as pd
import sys

allowed_pos= ['NN','VBD','VB','TO','JJ','CC','PRP$','VBP','PRP','NNS','DT']

trainingset=[]
cwd=os.getcwd()+'\\dataset\\1\\'
g=open(cwd+'POSGEN.json')
f =  json.load(g)
for i in f:
    #print i['POS'].keys()
    for unique in i['POS'].keys():
        if unique in allowed_pos:
            #print unique
            i[unique] = i['POS'][unique]
    i.pop('POS',None)
    i.pop('Review ',None)
    print i
        
    

with open(cwd + 'dataset_removed_POSGEN.json','w') as f1:
    json.dump(f,f1)
