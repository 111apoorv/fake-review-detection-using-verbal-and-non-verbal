import os
import json
import sklearn
import random
import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB, BernoulliNB,GaussianNB
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.linear_model import LogisticRegression,SGDClassifier,PassiveAggressiveClassifier
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import sys
import numpy as np


df = pd.read_csv('trainingdatanonverbal.csv')
test_df = pd.read_csv('testingset_nonverbal.csv')

##################################

training_set = df
testing_set = test_df

training_label = training_set['label']
testing_label = testing_set['label']


###################################
del training_set['label']
del testing_set['label']



#############################
clf0 = BernoulliNB()
print (len(training_set),len(testing_set))
clf0.fit(training_set,training_label)


clf01=MultinomialNB()
print (len(training_set),len(testing_set))
clf01.fit(training_set,training_label)





clf02 = GaussianNB()
print (len(training_set),len(testing_set))
clf02.fit(training_set,training_label)

clf1=LogisticRegression()
print (len(training_set),len(testing_set))
clf1.fit(training_set,training_label)

clf11=SGDClassifier()
print (len(training_set),len(testing_set))
clf11.fit(training_set,training_label)

clf2 = SVC()
print (len(training_set),len(testing_set))
clf2.fit(training_set,training_label)

clf21=LinearSVC()
print (len(training_set),len(testing_set))
clf21.fit(training_set,training_label)


clf3 = RandomForestClassifier(n_estimators=10)
print (len(training_set),len(testing_set))
clf3.fit(training_set,training_label)

clf4 = tree.DecisionTreeClassifier()
print (len(training_set),len(testing_set))
clf4.fit(training_set,training_label)


predict = clf0.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Bernoulli NaiveBayes accuracy:', accuracy)

predict = clf01.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Multinomial NaiveBayes accuracy:', accuracy)

predict = clf02.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Gausian NaiveBayes accuracy:', accuracy)


predict = clf1.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Logistic regression accuracy:', accuracy)

predict = clf11.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('SGDC classifier accuracy:', accuracy)


predict = clf2.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Support vector classification accuracy:', accuracy)

predict = clf21.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Linear Support vector classification accuracy:', accuracy)


predict = clf3.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Random Forest classification accuracy:', accuracy)

 
predict = clf4.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Decision Tree  accuracy:', accuracy)

 

