import os
import json
import sklearn
import random
import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB, BernoulliNB,GaussianNB
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.linear_model import LogisticRegression,SGDClassifier,PassiveAggressiveClassifier
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import average_precision_score
from sklearn.cluster import KMeans

import pandas as pd
import sys
import numpy as np
from sklearn.utils import shuffle

trainingset=[]
cwd=os.getcwd()+'\\dataset\\'
g1=open(cwd+'0\\dataset_removed_POS.json')
g2=open(cwd+'1\\dataset_removed_POSGEN.json')


df = pd.read_json(g1)
df.fillna(0,inplace=True)
df['label'] = 0
df.to_csv('fakereviews.csv', sep='\t', encoding='utf-8')

df2=pd.read_json(g2)
df2.fillna(0,inplace=True)
df2['label'] = 1
df2.to_csv('genuinereviews.csv', sep='\t', encoding='utf-8')
for i in df2.keys():
    df2[i] = df2[i].replace([''],0)




training_set = df[:6000]
testing_set = df[6000:]


training_set = training_set.append(df2[:4000])
training_set=shuffle(training_set)

training_set.to_csv('training_set.csv',sep='\t',encoding='utf-8')
testing_set = testing_set.append(df2[4000:])
testing_set=shuffle(testing_set)
testing_set.to_csv('testing_set.csv',sep='\t',encoding='utf-8')
##################################

training_label = training_set['label']
testing_label = testing_set['label']


###################################
del training_set['label']
del testing_set['label']
del training_set['Id']
del testing_set['Id']

print training_set.keys()
#############################
clf0 = BernoulliNB()
print (len(training_set),len(testing_set))
clf0.fit(training_set,training_label)


clf01=MultinomialNB()
print (len(training_set),len(testing_set))
clf01.fit(training_set,training_label)


clf02 = GaussianNB()
print (len(training_set),len(testing_set))
clf02.fit(training_set,training_label)

clf1=LogisticRegression()
print (len(training_set),len(testing_set))
clf1.fit(training_set,training_label)

clf11=SGDClassifier()
print (len(training_set),len(testing_set))
clf11.fit(training_set,training_label)

clf2 = SVC()
print (len(training_set),len(testing_set))
clf2.fit(training_set,training_label)

clf21=LinearSVC()
print (len(training_set),len(testing_set))
clf21.fit(training_set,training_label)

clf3 = RandomForestClassifier(n_estimators=10)
print (len(training_set),len(testing_set))
clf3.fit(training_set,training_label)

clf4 = tree.DecisionTreeClassifier()
print (len(training_set),len(testing_set))
clf4.fit(training_set,training_label)

predict = clf0.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)


     
print ('Bernoulli NaiveBayes accuracy:', accuracy)
predict = clf01.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Multinomial NaiveBayes accuracy:', accuracy)

predict = clf02.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Gausian NaiveBayes accuracy:', accuracy)


predict = clf1.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Logistic regression accuracy:', accuracy)

predict = clf11.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('SGDC classifier accuracy:', accuracy)


predict = clf2.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Support vector classification accuracy:', accuracy)

predict = clf21.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Linear Support vector classification accuracy:', accuracy)


predict = clf3.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Random Forest classification accuracy:', accuracy)

 
predict = clf4.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('Decision Tree  accuracy:', accuracy)



X=training_set
kmeans = KMeans(n_clusters=2,random_state=0).fit(X)
#kmeans.labels_array(training_label)
predict = kmeans.predict(testing_set)
accuracy = accuracy_score(predict,testing_label)
print ('kmeans CLASSIFICATION',accuracy)
